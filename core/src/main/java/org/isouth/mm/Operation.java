package org.isouth.mm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

public class Operation implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    private String name;
    private Class<?>[] params;
    private Class<?> answer;
    private Map<String, Object> attributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<?>[] getParams() {
        if (params == null) {
            params = ArrayUtils.EMPTY_CLASS_ARRAY;
        }
        return params;
    }

    public void setParams(Class<?>[] params) {
        this.params = params;
    }

    public Class<?> getAnswer() {
        return answer;
    }

    public void setAnswer(Class<?> answer) {
        this.answer = answer;
    }

    public Map<String, Object> getAttributes() {
        if (attributes == null) {
            attributes = new HashMap<>();
        }
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Object getAttribute(String key) {
        return getAttributes().get(key);
    }

    public void setAttribute(String key, Object value) {
        getAttributes().put(key, value);
    }

}
