package org.isouth.mm;

import akka.actor.UntypedActor;

/**
 * Worker actor
 * 
 * Receive all message from agent or adapter, dispatch to driver actor or reply
 * result back.
 * 
 * @author qiyi
 * 
 */
public class Worker extends UntypedActor {

    // TODO add extension feature
    @Override
    public void onReceive(Object input) throws Exception {
        Message msg = (Message) input;
        int status = msg.getStatus();
        switch (status) {
        case Message.REQUEST:
            MM.getExecutor().tell(msg, getSender());
            break;
        case Message.REPLY:
        case Message.FAIL:
            getSender().tell(msg, getSelf());
            break;
        default:
            unhandled(msg);
        }

    }
}
