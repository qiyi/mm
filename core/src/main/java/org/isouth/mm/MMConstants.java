package org.isouth.mm;

public abstract class MMConstants {
    public static final String SYSTEM_NAME = "MM";
    public static final String MODULE_NAME = SYSTEM_NAME.toLowerCase();
    public static final String DELIMITER = ".";
    public static final String MM_PREFIX = MODULE_NAME + DELIMITER;
}
