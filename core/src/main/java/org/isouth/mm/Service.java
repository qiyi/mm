package org.isouth.mm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * MM service definition
 * 
 */
public class Service implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String author;
    private String version;
    private Set<Operation> operations;
    private String type;
    private int status;
    private Map<String, Object> attributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Set<Operation> getOperations() {
        if (operations == null) {
            operations = new HashSet<>();
        }
        return operations;
    }

    public void setOperations(Set<Operation> operations) {
        if (operations == null) {
            operations = new HashSet<>();
        }
        this.operations = operations;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, Object> getAttributes() {
        if (attributes == null) {
            attributes = new HashMap<String, Object>();
        }
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Object getAttribute(String key) {
        return getAttributes().get(key);
    }

    public void setAttribute(String key, Object value) {
        getAttributes().put(key, value);
    }

}
