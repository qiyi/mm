package org.isouth.mm.pojo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Implement service meta definition
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MMServiceMeta {

    String author() default "";

    String version() default "1.0";

    /** default async mode **/
    boolean async() default true;

    String init() default "";

    String destroy() default "";

}
