package org.isouth.mm.pojo;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;
import org.isouth.mm.MMException;
import org.isouth.mm.Operation;
import org.isouth.mm.Registration;
import org.isouth.mm.Service;
import org.isouth.mm.pojo.annotation.MMServiceMeta;
import org.isouth.mm.util.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class PojoAgent {

    private static final Logger LOG = LoggerFactory.getLogger(PojoAgent.class);

    private static Map<Class<?>, Object> serviceCache = new ConcurrentHashMap<>();

    /**
     * get a service reference
     *
     * @param serviceIntf
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T refService(Class<T> serviceIntf) {
        if (!serviceCache.containsKey(serviceIntf)) {
            synchronized (serviceCache) {
                if (!serviceCache.containsKey(serviceIntf)) {
                    PojoWrapper<T> wrapper = new PojoWrapper<>(serviceIntf);
                    wrapper.init();
                    Enhancer e = new Enhancer();
                    e.setSuperclass(serviceIntf);
                    e.setCallbacks(new Callback[]{wrapper, NoOp.INSTANCE});
                    e.setCallbackFilter(wrapper);
                    serviceCache.put(serviceIntf, e.create());
                }
            }
        }
        return (T) serviceCache.get(serviceIntf);
    }

    /**
     * publish pojo service to registration
     *
     * @param serviceClazz
     * @param interfaceClazz
     * @param <T>
     */
    public static <T> void publish(Class<? extends T> serviceClazz, Class<T> interfaceClazz) {
        T serviceInstance;
        try {
            serviceInstance = serviceClazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOG.error("Create instance failed. class=" + serviceClazz);
            throw new MMException("Create instance failed, class=" + serviceClazz, e);
        }
        publish(serviceInstance, interfaceClazz);
    }

    /**
     * publish pojo service to registration
     *
     * @param implService
     * @param intfClazz
     * @param <T>
     */
    public static <T> void publish(T implService, Class<T> intfClazz) {

        Service service = PojoUtils.toService(intfClazz);
        service.setAttribute(PojoConstants.IMPL_SERVICE, implService);
        Set<Operation> operations = service.getOperations();
        Class<?> implClazz = implService.getClass();
        operations.forEach(operation -> {
            Method defMethod = (Method) operation.getAttribute(PojoConstants.DECLARE_METHOD);
            operation.setAttribute(PojoConstants.IMPL_METHOD, ClassUtils.searchMethod(implClazz, defMethod));
        });

        MMServiceMeta mwServiceMeta = implClazz.getAnnotation(MMServiceMeta.class);
        if (mwServiceMeta != null) {
            service.setAuthor(mwServiceMeta.author());
            service.setVersion(mwServiceMeta.version());
            String initMethodName = mwServiceMeta.init();
            Method initMethod = ClassUtils.findMethod(implClazz, initMethodName);
            String destroyMethodName = mwServiceMeta.destroy();
            Method destroyMethod = ClassUtils.findMethod(implClazz, destroyMethodName);
            if (initMethod != null) {
                ClassUtils.invoke(initMethod, implService, (Object[]) null);
                service.setAttribute(PojoConstants.INIT_METHOD, initMethod);
            }
            if (destroyMethod != null) {
                service.setAttribute(PojoConstants.DESTROY_METHOD, destroyMethod);
            }
        }

        Registration.get().publish(service);

    }
}
