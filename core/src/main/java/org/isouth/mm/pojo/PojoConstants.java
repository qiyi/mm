package org.isouth.mm.pojo;

import static org.isouth.mm.MMConstants.*;

public abstract class PojoConstants {
    public static final String INIT_METHOD = MM_PREFIX + "service.init";
    public static final String DESTROY_METHOD = MM_PREFIX + "service.destroy";
    public static final String DECLARE_METHOD = MM_PREFIX + "oper.def";
    public static final String IMPL_METHOD = MM_PREFIX + "oper.impl";
    public static final String DECLARE_SERVICE = MM_PREFIX + "service.def";
    public static final String IMPL_SERVICE = MM_PREFIX + "service.impl";
}
