package org.isouth.mm.pojo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import net.sf.cglib.proxy.CallbackFilter;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import org.isouth.mm.MM;
import org.isouth.mm.MMException;
import org.isouth.mm.Message;
import org.isouth.mm.Operation;
import org.isouth.mm.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.concurrent.Await;
import scala.concurrent.Future;
import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;

public class PojoWrapper<T> implements MethodInterceptor, CallbackFilter {
    private static final Logger LOG = LoggerFactory.getLogger(PojoWrapper.class);
    private Service service;
    private Map<Method, Operation> method2Opers;

    public PojoWrapper(Class<T> intfClazz) {
        this.service = PojoUtils.toService(intfClazz);
    }

    public void init() {
        Set<Operation> operations = this.service.getOperations();
        method2Opers = new HashMap<Method, Operation>(operations.size());
        operations.forEach(operation -> {
            method2Opers.put((Method) operation.getAttribute(PojoConstants.DECLARE_METHOD), operation);
        });
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        Operation operation = method2Opers.get(method);
        if (operation == null) {
            throw new UnsupportedOperationException();
        }

        // 调用Actor，发送消息
        Message msg = new Message();
        msg.setService(service.getName());
        msg.setOperation(operation);
        msg.setArgs(args);
        msg.setStatus(Message.REQUEST);

        Timeout timeout = new Timeout(TimeUnit.SECONDS.toMillis(3));
        ActorRef worker = MM.getWorker();
        Future<Object> future = Patterns.ask(worker, msg, timeout);
        Message resultMsg = (Message) Await.result(future, timeout.duration());
        if (resultMsg.getStatus() == Message.FAIL) {
            LOG.error("Execute service fail, service name is " + service.getName());
            Throwable e = resultMsg.getError() != null ? resultMsg.getError() : new MMException(
                    "Invoke serivce fail. service is " + service.getName());
            throw e;
        }
        return resultMsg.getResult();

    }

    @Override
    public int accept(Method method) {
        return method2Opers.containsKey(method) ? 0 : 1;
    }

}
