package org.isouth.mm.pojo;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;

import org.isouth.mm.MMException;
import org.isouth.mm.Operation;
import org.isouth.mm.Service;
import org.isouth.mm.pojo.annotation.MMOperation;
import org.isouth.mm.pojo.annotation.MMService;

public class PojoUtils {
    public static Service toService(Class<?> intfClazz) {
        Service service = new Service();
        service.setType(PojoAdapter.POJO_TYPE);
        MMService mwService = intfClazz.getAnnotation(MMService.class);
        if (mwService == null) {
            throw new MMException("Invalid intf service.");
        }
        service.setName(mwService.name());
        service.setAttribute(PojoConstants.DECLARE_SERVICE, intfClazz);
        Set<Operation> operations = service.getOperations();
        Method[] intfMethods = intfClazz.getDeclaredMethods();
        for (Method intfMethod : intfMethods) {
            MMOperation mwOper = intfMethod.getAnnotation(MMOperation.class);
            if (mwOper != null) {
                Operation operation = new Operation();
                operation.setName(mwOper.name());
                Map<String, Object> operAttrs = operation.getAttributes();
                operAttrs.put(PojoConstants.DECLARE_METHOD, intfMethod);
                operations.add(operation);
            }
        }
        return service;
    }
}
