package org.isouth.mm.pojo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.isouth.mm.Adapter;
import org.isouth.mm.MMException;
import org.isouth.mm.Message;
import org.isouth.mm.Operation;
import org.isouth.mm.Service;

import com.google.common.collect.ImmutableSet;

/**
 * POJO service adapter
 * 
 * @author qiyi
 * 
 */
public class PojoAdapter implements Adapter {
    // private final Logger log = LoggerFactory.getLogger(PojoConnector.class);
    public static final String POJO_TYPE = "Pojo";
    private Map<String, Service> serviceMap = new ConcurrentHashMap<>();

    @Override
    public String getType() {
        return POJO_TYPE;
    }

    @Override
    public void addService(Service service) {
        serviceMap.put(service.getName(), service);
    }

    @Override
    public void rmService(Service service) {
        serviceMap.remove(service.getName());
    }

    @Override
    public Set<Service> getAllService() {
        return ImmutableSet.copyOf(serviceMap.values());
    }

    @Override
    public void handle(Message msg) {
        String reqService = msg.getService();
        Operation reqOper = msg.getOperation();
        Service service = serviceMap.get(reqService);
        if (service == null) {
            throw new MMException("Service not exist, service name is " + reqService);
        }
        Operation operation = findOperation(service, reqOper);

        Object obj = service.getAttribute(PojoConstants.IMPL_SERVICE);
        Method method = (Method) operation.getAttribute(PojoConstants.IMPL_METHOD);
        try {
            Object result = method.invoke(obj, msg.getArgs());
            msg.setResult(result);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new MMException(e);
        }

    }

    private Operation findOperation(Service service, Operation reqOper) {
        for (Operation oper : service.getOperations()) {
            if (oper.getName().equals(reqOper.getName())) {
                return oper;
            }
        }
        return null;

    }

}
