package org.isouth.mm;

import java.util.Set;

/**
 * An adapter manages special type of service and the operations in a service,
 * usually the adapter will proxy the service invoke.
 * 
 * @author qiyi
 * 
 */
public interface Adapter {

    /**
     * Get the adapter type, such as: POJO, HTTP
     * 
     * @return
     */
    String getType();

    /**
     * Add a service to adapter
     * 
     * @param service
     */
    void addService(Service service);

    /**
     * Remove a service from adapter
     * 
     * @param service
     */
    void rmService(Service service);

    /**
     * Get all services in adapter
     * 
     * @return
     */
    Set<Service> getAllService();

    /**
     * Handle a message to invoke a service
     * 
     * @param msg
     */
    void handle(Message msg);
}
