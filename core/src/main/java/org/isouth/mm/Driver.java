package org.isouth.mm;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import akka.actor.UntypedActor;

/**
 * Service driver
 * 
 * Receive message from worker actor and choose target adapter to handle the
 * message.
 * 
 * @author qiyi
 * 
 */
public class Driver extends UntypedActor {

    @Override
    public void onReceive(Object arg0) throws Exception {
        Message msg = (Message) arg0;
        if (msg.getStatus() == Message.REQUEST) {
            String serviceName = msg.getService();
            Registration registration = Registration.get();
            Set<Service> findServices = registration.getService(serviceName);
            if (CollectionUtils.isEmpty(findServices)) {
                replyFail(msg, null, "Service not exist, service is " + serviceName);
                return;
            }
            Service service = findServices.iterator().next();
            Adapter adapter = registration.findAdapter(service);
            if (adapter == null) {
                replyFail(msg, null, "Service adapter not exist, service is " + serviceName);
                return;
            }
            try {
                adapter.handle(msg);
            } catch (Exception e) {
                replyFail(msg, e, null);
                return;
            }
            msg.setStatus(Message.REPLY);
            getSender().tell(msg, getSelf());
        } else {
            unhandled(msg);
        }
    }

    private void replyFail(Message msg, Throwable e, String errorMsg) {
        msg.setStatus(Message.FAIL);
        if (e != null) {
            msg.setError(e);
        } else if (errorMsg != null) {
            msg.setError(new MMException(errorMsg));
        }
        getSender().tell(msg, getSelf());
    }

}
