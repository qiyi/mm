package org.isouth.mm.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.isouth.mm.MMException;

public class ClassUtils {

    public static Method searchMethod(Class<?> implClazz, Method intfMethod) {
        try {
            return implClazz.getDeclaredMethod(intfMethod.getName(), intfMethod.getParameterTypes());
        } catch (NoSuchMethodException | SecurityException e) {
            throw new MMException(e);
        }
    }

    public static Method findMethod(Class<?> clazz, String methodName) {
        if (StringUtils.isBlank(methodName)) {
            throw new MMException("NoSuchMethod " + methodName);
        }
        try {

            return clazz.getMethod(methodName, (Class<?>[]) null);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new MMException(e);
        }
    }

    public static Object invoke(Method method, Object obj, Object... args) {
        try {
            return method.invoke(obj, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new MMException(e);
        }

    }
}
