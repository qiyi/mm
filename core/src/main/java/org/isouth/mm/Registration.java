package org.isouth.mm;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections.SetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Service registration center
 *
 * @author qiyi
 */
public class Registration {
    private final Logger log = LoggerFactory.getLogger(Registration.class);
    private static Registration registration = new Registration();
    private Map<String, Set<Service>> services = new ConcurrentHashMap<>();
    private Map<Service, Adapter> serviceAdapters = new ConcurrentHashMap<>();
    private BiMap<String, Adapter> adapters = HashBiMap.create();

    public static Registration get() {
        return registration;
    }

    /**
     * Get all services from registration
     *
     * @return all services
     */
    public Set<Service> findAllService() {
        return null;
    }

    public Set<Service> getService(String name) {
        return services.get(name);
    }

    /**
     * Get all services have the same name
     *
     * @param name service name
     * @return answer service
     */
    public Set<Service> findService(String name) {
        Set<Service> allServices = services.get(name);
        @SuppressWarnings("unchecked")
        Set<Service> answer = allServices == null ? SetUtils.EMPTY_SET : ImmutableSet.copyOf(allServices);
        return answer;
    }

    /**
     * publish a service
     *
     * @param service the service to publish
     */
    public void publish(Service service) {
        String serviceName = service.getName();
        // check if service exist
        Set<Service> allServices = services.get(serviceName);
        if (allServices == null) {
            allServices = new HashSet<>();
            services.put(serviceName, allServices);
        }
        if (allServices.contains(serviceName)) {
            log.warn("Service already exist, service is {}.", service);
            return;
        }

        // add new service
        // ensure the connector exist
        String serviceType = service.getType();
        Adapter adapter = adapters.get(serviceType);
        if (adapter == null) {
            log.error("Service type not supported yet, service is {}, type is {}.", serviceName, serviceType);
            return;
        }

        allServices.add(service);
        adapter.addService(service);
        serviceAdapters.put(service, adapter);
    }

    public Service unpublish(Service service) {
        // TODO unpublish service
        return service;
    }

    /**
     * add adapter to framework
     *
     * @param adapter to add adapter
     */
    public void addAdapter(Adapter adapter) {
        adapters.putIfAbsent(adapter.getType(), adapter);
    }

    public void rmAdapter(Adapter adapter) {
        adapters.remove(adapter.getType());
    }

    public Adapter getAdapter(String type) {
        return adapters.get(type);
    }

    public Adapter findAdapter(Service service) {
        return serviceAdapters.get(service);
    }
}
