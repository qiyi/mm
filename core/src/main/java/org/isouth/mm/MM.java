package org.isouth.mm;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class MM {

    private static MM mm = new MM();

    private ActorSystem system = ActorSystem.create("MM");

    private Props workerProps = Props.create(Worker.class);

    private Props executorProps = Props.create(Driver.class);

    public static ActorRef getWorker() {
        return mm.system.actorOf(mm.workerProps);
    }

    public static ActorRef getExecutor() {
        return mm.system.actorOf(mm.executorProps);
    }

}
