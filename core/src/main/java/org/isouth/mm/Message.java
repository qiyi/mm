package org.isouth.mm;

import java.io.Serializable;

public class Message implements Serializable, Cloneable {
    public static final int REQUEST = 0;
    public static final int REPLY = 1;
    public static final int FAIL = 2;
    private static final long serialVersionUID = 1L;
    private String service;
    private Operation operation;
    private int status;
    private Object[] args;
    private Throwable error;
    private String errorMsg;
    private Object result;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
