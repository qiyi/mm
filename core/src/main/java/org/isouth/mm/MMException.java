package org.isouth.mm;

public class MMException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MMException() {
        super();
    }

    public MMException(String msg) {
        super(msg);
    }

    public MMException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MMException(Throwable cause) {
        super(cause);
    }

}
